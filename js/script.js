$( document ).ready(function() {
	$('#submit-btn').click( function(event) {
		event.preventDefault
		$('#result').removeClass('error');
		$('#result').removeClass('green');
		$('#result').html('Loading...');;
		$.ajax({
		    url: 'result.php',
		    type: 'post',
		    data: $('#cardform').serialize(),
		    success: function(data) {
                var obj = jQuery.parseJSON(data); 
				if(obj.success == true){
					$('#result').addClass('green');
				}else{
					$('#result').removeClass('green');
					$('#result').addClass('error');
				}
				$('#result').html(obj.result);
				if(obj.card1){
					$('#card1').val(obj.card1);
				}
				if(obj.card2){
					$('#card2').val(obj.card2);
				}
            },
            error: function(){
                  alert('error');
            }
		});
	});

});