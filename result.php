<?php
use classes\SimpleValidation;
use classes\Card;
/*
* Loading classes
*/
spl_autoload_register(function ($class) {
	$class = str_replace("\\",'/',$class);
	include(__DIR__ . "/" . $class . ".class.php");
});

if(isset($_POST) && count($_POST) > 0 ){
 	$arr = array('success'=>false ,
				  'result'=>'Some error occured');

	try {
		$totalValue =0 ;
		$cards=null;
		$tempCardsArr =  array();
		foreach($_POST['cards'] as $key=>$val){
			//Simply Validating cards
			new  SimpleValidation($val , $key+1);
			//Initializing Card class and validating against rules
			$cards = new Card($val , $key+1);
			$totalValue += $cards->getCardValue();  
			$tempCardsArr[] = $cards->getCardValues();
		}
		//checking the same value for both cards 
		if ($tempCardsArr[0] ===  $tempCardsArr[1] ) {
			throw new \Exception('Both cards faces and suits are same. <BR>Card values are : ' . $totalValue);
		}
		//creating array for result
		$arr = array('success'=>true ,
					  'result'=>'Card values are : ' . $totalValue , 
					  'card1' => $tempCardsArr[0],
					  'card2' => $tempCardsArr[1]	);
		
	} catch(Exception $e) { 
		//Errot handling

		//creating array for result
		$arr = array('success'=>false ,
					  'result'=>$e->getMessage(),
					 );
		if(isset($tempCardsArr[0])){
			$arr['card1'] = $tempCardsArr[0];
		}
		if(isset($tempCardsArr[1])){
			$arr['card2'] = $tempCardsArr[1];
		}
	}
	//result in json 
	echo json_encode($arr);
}