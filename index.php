<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title> </title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.css">
	<!-- Latest Jquery  -->
	<script src="//code.jquery.com/jquery-latest.js"></script>

	<script src="js/script.js"></script>
	<style>
		body {
	      padding-top: 60px; 
	    }
		header{ color:#fff;}
		.form-cards {
		    max-width: 600px;
			min-width: 600px;
		    padding: 19px 29px 29px;
		    margin: 0 auto 20px;
		    background-color: #fff;
		    border: 1px solid #e5e5e5;
		    -webkit-border-radius: 5px;
		       -moz-border-radius: 5px;
		            border-radius: 5px;
		    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
		       -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
		            box-shadow: 0 1px 2px rgba(0,0,0,.05);
		 }
		.form-cards .form-cards-heading,
		.form-cards .checkbox {
		  margin-bottom: 10px;
		}
		.form-cards input[type="text"],
		.form-cards input[type="password"] {
		  font-size: 16px;
		  height: auto;
		  margin-bottom: 15px;
		  padding: 7px 9px;
		}
		.result{
			font-size:18px;
			margin-top:50px;
		}
		.center{
			text-align :center;
		}
		.error{
			color:#ff0000;
		}
		.green{
			color:#008000;
		}
	
	</style> 
  </head>
  <body>
	 <header>
		<div class="navbar navbar-inverse navbar-fixed-top">
		  <div class="navbar-inner">
			<div class="container">
			   
			  <h3>finder.com.au-Test</h3>
			   
			</div>
		  </div>
		</div>
	 </header>
     <div class="container">

        <p></p>
		<p>Problem 1: Write a program which accepts two inputs, representing two playing cards out of a standard 52 card deck. Add these two cards together to produce a blackjack score, and print the score on the screen for the output.</p>
		<p>Cards will be identified by the input, the first part representing the face value from 2-10, plus A, K, Q, J. The second part represents the suit S, C, D, H.</p>
		<p>The blackjack score is the face value of the two cards added together, with cards 2-10 being the numeric face value, and A is worth 11, and K, Q, J are each worth 10.		</p>
		<form class="form-cards" id="cardform">
		    <h3 class="form-cards-heading">Please Choose 2 cards face value followed by suit value </h3>
		    <h4 >Face Value  : 2-10, plus A, K, Q, J</h4>
		    <h4 >Suite Value : S, C, D, H</h4>
			<div class="center">
				<label>Card 1  </label><input type="text" id="card1"  name="cards[]"  class="input-block-level" placeholder="e.g: 5D"><BR>
				<label>Card 2  </label><input type="text" id="card2"  name="cards[]"  class="input-block-level" placeholder="e.g:AH"><BR><BR>
				<button class="btn btn-large btn-primary " type="button" id="submit-btn">Submit</button>
			</div>		    
			<div id="result" class="center result green"> </div>
      	</form>
    </div> <!-- /container -->
	</body>
</html>
