<?php
/**
 * Card Class
 *
 * PHP version 5
 *
 * @package    Card
 * @author     Adil Yaqoob <adil@adil.com.au>
 * @version    SVN: $Id$
 */

namespace classes;
class Card
{
	
	// suits:  'S', 'C', 'D', 'H'
	private $_suit;

	// faces : 2, 3, 4, 5, 6, 7, 8, 9, 10, J, Q, K, A
	private $_face;

	private $_suits = array('S', 'C', 'D', 'H');

	private $_faces = array('2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A');
	
	private $_cardnumber;
	
	private $_cardvalue;
	
	/**
       * constructor to create new card
       *
       * @param string $value  Value of the card
       * @param string $number  Number of the card
       * @return 
    */
	public function __construct($value, $number) {
		$this->_cardnumber = $number;
		$this->_cardvalue = trim($value);
		$face = $this->getFaceValue();
		$suit = $this->getSuitValue();
		$this->_cardvalue = $this->getFace().$this->getSuit();
	}


	/**
	* Get suit of the card.
	* @return suit of the card.
	*/
	public function getSuit() {
		return $this->_suit;
	}

	/**
	* Get face of the card.
	* @return face of the card.
	*/
	public function getFace() {
		return $this->_face;
	}


	/**
	* Set suit of the card.
	* @param string $value  Value of the suit
	* @return  
	*/
	public function setSuit($value) {
		$this->_suit = strtoupper($value);
	}

	/**
	* Get face of the card.
	* @param string $value  Value of the face
	* @return 
	*/
	public function setFace($value) {
		$this->_face = strtoupper($value);
	}

	/**
	 * return  card value with face and suit
	 * @param 
	 * @return card value with face and suit
	 */
	public function getCardValues() {
		return $this->_cardvalue;
	}
		
	/**
	* Check if suit value is valid.
	* @param suit value
	* @return True if suit value is valid.
	*/
	public function isValidSuitValue($suit) {
		$suits = $this->_suits;
		return in_array(strtoupper($suit), $suits);
	}

	/**
	* Check if face value is valid.
	* @param face value
	* @return True if face value is valid.
	*/
	public function isValidFaceValue($face) {
		$faces =  $this->_faces;
		return in_array(strtoupper($face), $faces);
	}
	
	/**
	 * separate the suit value from face/suit value.
	 * @param 
	 * @return 
	 */
	public function getSuitValue() {
		$value=$this->_cardvalue;
		$suit = substr($value, -1);
		if (!$this->isValidSuitValue($suit)) {
			throw new \Exception('Suit value is not valid for card ' . $this->_cardnumber);
		}
		$this->setSuit($suit);
	}
	
	/**
	 * separate the face value from face/suit value.
	 * @param
	 * @return
	 */
	public function getFaceValue() {
		$value= $this->_cardvalue;
		$face = substr($value, 0, -1);
		if (!$this->isValidFaceValue($face)) {
			throw new \Exception('Face value is not valid for card ' .$this->_cardnumber);
		}
		$this->setFace($face);
	}
	
	/**
	 * to get the card face value for total
	 * @param
	 * @return the value of the face
	 */
	public function getCardValue() {
		switch ($this->_face) {
			case 'J':
			case 'Q':
			case 'K':
				$value = 10;
				break;
			case 'A' :
				$value = 11;
				break;
			default:
				$value = $this->_face;
				break;
		}
		return $value;
	}
}