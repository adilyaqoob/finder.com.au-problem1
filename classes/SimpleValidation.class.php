<?php

/**
 * Simple validation
 *
 * PHP version 5
 *
 * @package    SimpleValidation
 * @author     Adil Yaqoob <adil@adil.com.au>
 * @version    SVN: $Id$
*/

namespace classes;
class SimpleValidation
{
	public function __construct($value,$number ) {
		if(strlen(trim($value)) == ''){
			 throw new \Exception('Card '.$number. ' value is empty');
		}
	}

} 
